import matplotlib.pyplot as plt
import matplotlib.cm as cm

import numpy as np
import pandas as pd

from sklearn.cluster import KMeans, DBSCAN
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn import preprocessing

from statsmodels.tsa.stattools import coint

from scipy import stats

import warnings

warnings.filterwarnings("ignore")

##########
# Dokumentation der Variablen
##########

# stockdata: dataframe, indiziert über date, Spalten sind Sktienkurse der Verschiedenen aktien
# returns: dataframe, indiziert über date, Spalten sind prozentuale Veränderung zum Vortag der verschiedenen Aktien
# data: dataframe auf den das Clustering angewendet werden soll
# pairs: Sammlung aller gefundenen kointegrierten Paare
# S1/S2: Preisdaten, der 1. bzw. 2 Aktie im ausgewählten Paar

#### Dataframe mit Aktiendaten einlesen

stockdata = pd.read_csv('S&P500_stock_data.csv')

stockdata['date'] = pd.to_datetime(stockdata['date'])
# stockdata.index = stockdata['date']
stockdata.drop(columns=['date'], inplace=True)

returns = stockdata.pct_change()
returns = returns.iloc[1:, :].dropna(axis=1)

#### Aufteilen in Trainings- und Testdaten

data = returns.copy()

trainingsanteil = 0.7
split = int(len(data) * trainingsanteil)

train = data[:split]
test = data[split:]

data = train

#### PCA Decomposition und Clustering

N_PRIN_COMPONENTS = 50
pca = PCA(n_components=N_PRIN_COMPONENTS)
pca.fit(data)

X = pca.components_.T
X = preprocessing.StandardScaler().fit_transform(X)

clf = DBSCAN(eps=3, min_samples=3)
clf.fit(X)
labels = clf.labels_

n_clusters = len(set(labels)) - (1 if -1 in labels else 0)

# print("\nClusters discovered: %d" % n_clusters_)

clustered = clf.labels_

clustered_series = pd.Series(index=returns.columns, data=clustered.flatten())
clustered_series_all = pd.Series(index=returns.columns, data=clustered.flatten())
clustered_series = clustered_series[clustered_series != -1]

# Output zu Clustergrößen und Anzahl an Paaren

CLUSTER_SIZE_LIMIT = 9999
counts = clustered_series.value_counts()
ticker_count_reduced = counts[(counts > 1) & (counts <= CLUSTER_SIZE_LIMIT)]
print("Clusters formed: %d" % len(ticker_count_reduced))
print("Pairs to evaluate: %d" % (ticker_count_reduced * (ticker_count_reduced - 1)).sum())


# Cointegration Relationships

def find_cointegrated_pairs(data, significance=0.05):
    n = data.shape[1]
    score_matrix = np.zeros((n, n))
    pvalue_matrix = np.ones((n, n))
    keys = data.keys()
    pairs = []
    for i in range(n):
        for j in range(i + 1, n):
            S1 = data[keys[i]]
            S2 = data[keys[j]]
            result = coint(S1, S2)
            score = result[0]
            pvalue = result[1]
            score_matrix[i, j] = score
            pvalue_matrix[i, j] = pvalue
            if pvalue < significance:
                pairs.append((keys[i], keys[j]))
    return score_matrix, pvalue_matrix, pairs


cluster_dict = {}
for i, which_clust in enumerate(ticker_count_reduced.index):
    tickers = clustered_series[clustered_series == which_clust].index
    score_matrix, pvalue_matrix, pairs = find_cointegrated_pairs(
        data[tickers]
    )
    cluster_dict[which_clust] = {}
    cluster_dict[which_clust]['score_matrix'] = score_matrix
    cluster_dict[which_clust]['pvalue_matrix'] = pvalue_matrix
    cluster_dict[which_clust]['pairs'] = pairs

pairs = []
for clust in cluster_dict.keys():
    pairs.extend(cluster_dict[clust]['pairs'])

print("We found %d pairs." % len(pairs))
print("In those pairs, there are %d unique tickers." % len(np.unique(pairs)))


#### Ratio- und Signalberechnung

def zscore(series):
    return (series - series.mean()) / np.std(series)


pair = pairs[1]
S1 = test[pair[0]]
S2 = test[pair[1]]

print(pair[0])
print(pair[1])

ratio = S1 / S2

test = ratio.copy()

# Berechnung einiger Features, um Signale zu geben

ratios_mavg5 = test.rolling(window=5, center=False).mean()
ratios_mavg60 = test.rolling(window=60, center=False).mean()
std_60 = test.rolling(window=60, center=False).std()
zscore_60_5 = (ratios_mavg5 - ratios_mavg60) / std_60

# Einfache Signale für jeden Tag: Buy, falls Abweichung größer 1, Sell, falls Abweichung kleiner 1

buy = test.copy()
sell = test.copy()

buy[zscore_60_5 > -1] = 0
sell[zscore_60_5 < 1] = 0

# print(buy)

label = np.empty(len(ratio), dtype=int)

label[buy != 0] = 1
label[sell != 0] = -1

backtest_data = pd.DataFrame()
backtest_data['price'] = ratio
backtest_data['labels'] = label
backtest_data['close'] = backtest_data['price']


#### Backtest

def run_backtest(data, cash):
    '''Function to run a backtest on one security

    Input:  dataframe with columns:
                    index: date (must be same in price und labels)
                    price: trade price of security to trade
                    labels: amount of securities to buy or sell at given point in time
            starting cash: amount of cash to start with

    Output: dataframe with columns:
                    index, price, label, pos, costbasis, posmtm, cash, nav
    '''

    # Erweiterungen: Mehrere Securities, Transaktionskosten, Short-Interest

    # calculations:    
    data['pos'] = data['labels'].cumsum()  # actual amount of securities held
    data['costbasis'] = data['close'] * data['labels']  # costbasis of new positions
    data['pos mtm'] = data['close'] * data['pos']  # marktomarket value of securities in portfolio
    data['cash'] = cash - data['costbasis'].cumsum()  # amount of cash
    data['nav'] = data['cash'] + data['pos mtm']  # net asset value of portfolio

    return data


data = run_backtest(backtest_data, 100)

print(data)


def plot_backtest(data):
    '''Function for backtest plotting

    Input: dataframe from run_backtest() function
    '''
    date = data.index

    fig, axs = plt.subplots(3, 1, layout='constrained', gridspec_kw={'height_ratios': [5, 5, 2]}, sharex=True)

    # set titles of subplots
    axs[0].set_title('Portfolio', loc='left')
    axs[1].set_title('Securities', loc='left')
    axs[1].set_title('Positions', loc='left')

    # plot portfolio positions
    posmtm_plot, = axs[0].plot(date, data['pos mtm'], label='Pos MTM', color='brown')
    cash_plot, = axs[0].plot(date, data['cash'], label='cash', color='lightskyblue')
    nav_plot, = axs[0].plot(date, data['nav'], label='nav', color='blue')
    axs[0].legend(handles=[posmtm_plot, cash_plot, nav_plot], loc='lower left')

    # plot securities
    sec_plot, = axs[1].plot(date, data['close'], label='AAPL close', color='blue')
    axs[1].legend(handles=[sec_plot], loc='upper left')

    # plot positions
    pos_plot, = axs[2].plot(date, data['pos'], label='Positions', color='blue')

    plt.show()


plot_backtest(data)